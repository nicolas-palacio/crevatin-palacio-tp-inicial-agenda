package presentacion.vista;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class VentanaLogin extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldPassword;
	private JButton btnLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaLogin frame = new VentanaLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 394, 240);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnLogin = new JButton("Ingresar");
		btnLogin.setBounds(135, 135, 89, 23);
		contentPane.add(btnLogin);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a de PostgreSQL");
		lblContrasea.setBounds(43, 80, 155, 14);
		contentPane.add(lblContrasea);
		
		textFieldPassword = new JTextField();
		textFieldPassword.setColumns(10);
		textFieldPassword.setBounds(208, 77, 109, 20);
		contentPane.add(textFieldPassword);
	}
	
	public void mostrarVentana(boolean mostrar) {
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(mostrar);
		
	}
	


	public JTextField getTextFieldPassword() {
		return textFieldPassword;
	}

	public JButton getBtnLogin() {
		return btnLogin;
	}


	
	
}
