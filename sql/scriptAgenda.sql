CREATE TABLE IF NOT EXISTS personas(
  idPersona SERIAL PRIMARY KEY,
  nombre char(45),
  telefono char(15),
  correo char(40),
  fech_cumpleanios date,
  tipo char(20),
  pais char(20),
  provincia char(30),
  localidad char(30),
  calle char(30),
  altura int,
  depto char(10),
  signoZodiaco char(20),
  musicaPreferida char(30)
);



