package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.swing.JOptionPane;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import modelo.Agenda;
import presentacion.controlador.seguridad.Encriptacion;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaGenerarReporte;
import presentacion.vista.VentanaPersona;
import presentacion.vista.Vista;
import dto.PersonaDTO;

public class Controlador implements ActionListener {
	private Vista vista;
	private List<PersonaDTO> personasEnTabla;
	private VentanaPersona ventanaPersona;
	private VentanaGenerarReporte ventanaGenerarReporte;
	private Agenda agenda;
	private PersonaDTO personaEditar;

	public Controlador(Vista vista, Agenda agenda) {
		this.vista = vista;
		this.vista.getBtnAgregar().addActionListener(a -> ventanaAgregarPersona(a));
		this.vista.getBtnBorrar().addActionListener(s -> borrarPersona(s));
		this.vista.getBtnEditar().addActionListener(m -> editarPersona(m));
		this.vista.getBtnReporte().addActionListener(r -> mostrarVentanaReporte(r));
		this.ventanaPersona = VentanaPersona.getInstance();
		this.ventanaGenerarReporte= ventanaGenerarReporte.getInstance();

		this.ventanaPersona.getBtnAgregarPersona().addActionListener(p -> guardarPersona(p));
		this.ventanaPersona.getBtnLimpiarFecha().addMouseListener(limpiarCampoFechaEvnt());
		this.ventanaPersona.getTxtTelefono().addKeyListener(comprobarInput());
		this.ventanaPersona.getDateChooser().addPropertyChangeListener(comprobarSigno());
		this.ventanaPersona.getTextAltura().addKeyListener(comprobarInput());
		this.ventanaPersona.getBtnGuardarCambios().addActionListener(p -> guardarEdicionPersona(p));
		this.ventanaPersona.getComboBoxPais().addActionListener(p -> habilitarProvincias(p));
		this.ventanaPersona.getComboBoxProvincia().addActionListener(p -> cargarLocalidades(p));
		
		this.ventanaGenerarReporte.getBtnGenerar().addActionListener(p->mostrarReporte(p));
		this.ventanaGenerarReporte.getComboBoxTipoReporte().addActionListener(p->comprobarTipoReporte(p));
		
		cargarPaises();
		this.agenda = agenda;

	}

	private void ventanaAgregarPersona(ActionEvent a) {
		this.ventanaPersona.limpiarVentana();
		cargarSignosZodiaco();
		cargarPreferenciasMusicales();
		cargarProvincias();
		cargarTipo();
		this.ventanaPersona.mostrarBotonAgregar();
		this.ventanaPersona.mostrarVentana();
	}

	private void guardarPersona(ActionEvent p) {
		String nombre = this.ventanaPersona.getTxtNombre().getText();
		String tel = "";
		if (!this.ventanaPersona.getTxtTelefono().getText().isEmpty()) {
			tel = this.ventanaPersona.getTxtTelefono().getText();
		}

		String correo = this.ventanaPersona.getTextCorreo().getText();
		Date cumpleanios =new Date(0000,00,00);
		if(this.ventanaPersona.getDateChooser().getDate()!=null) {
			cumpleanios=this.ventanaPersona.getDateChooser().getDate();	
		}
		
		
		String tipo = this.ventanaPersona.getComboBoxTipo().getSelectedItem().toString();
		String pais = "";
		String provincia = "";
		String localidad = "";

		if (this.ventanaPersona.getComboBoxPais().getSelectedItem() != null) {
			pais = this.ventanaPersona.getComboBoxPais().getSelectedItem().toString();

		}

		if (this.ventanaPersona.getComboBoxProvincia().getSelectedItem() != null
				&& this.ventanaPersona.getComboBoxLocalidades().getSelectedItem() != null) {
			provincia = this.ventanaPersona.getComboBoxProvincia().getSelectedItem().toString();
			localidad = this.ventanaPersona.getComboBoxLocalidades().getSelectedItem().toString();

		}

		String calle ="";		
		if(this.ventanaPersona.getTextCalle().getText() != null) {
			calle= this.ventanaPersona.getTextCalle().getText();
		}
		
		Integer altura =-1;
		if(!this.ventanaPersona.getTextAltura().getText().isEmpty()) {
			System.out.println("Altura edit "+this.ventanaPersona.getTextAltura().getText());
			altura=Integer.parseInt(this.ventanaPersona.getTextAltura().getText());
		}
		
		String depto ="";
		if(this.ventanaPersona.getTextCalle().getText() != null) {
			depto= this.ventanaPersona.getTextDepto().getText();
		}
		
		String signo="No especificado";
		if (this.ventanaPersona.getComboBoxZodiacal().getSelectedItem() !="") {
			signo = this.ventanaPersona.getComboBoxZodiacal().getSelectedItem().toString();

		}
				
		String musicaPreferida="Otros";
		if (this.ventanaPersona.getComboBoxMusica().getSelectedItem() !="") {
			musicaPreferida = this.ventanaPersona.getComboBoxMusica().getSelectedItem().toString();

		}

		if (puedeGuardar(nombre, tel + "",correo,altura)) {
				
			PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, correo, cumpleanios, tipo, pais, provincia,
					localidad, calle, altura,depto,signo,musicaPreferida);

			this.agenda.agregarPersona(nuevaPersona);
			this.refrescarTabla();
			this.ventanaPersona.limpiarVentana();
		}
	}

	private boolean puedeGuardar(String nombre, String telefono,String correo, int altura) {
		boolean result=true;
		
		if (nombre.isEmpty() || nombre.length() > 45) {
			JOptionPane.showMessageDialog(null, "El campo nombre no puede estar vacio ni superar los 45 caracteres.",
					"Alerta", 1);
			result= false;
		}

		if (telefono.isEmpty() || telefono.length()>14) {
			JOptionPane.showMessageDialog(null, "El campo telefono no puede estar vacio ni superar los 14 caracteres.",
					"Alerta", 1);
			result= false;
		}
		
		if (correo.isEmpty() || correo.length() > 40) {
			JOptionPane.showMessageDialog(null, "El campo correo no puede estar vacio ni superar los 40 caracteres",
					"Alerta", 1);
			result= false;
		}
		
		String alturaString=altura+"";	
		if(alturaString.length()>8) {
			JOptionPane.showMessageDialog(null, "El campo altura no puede superar los 8 caracteres.",
					"Alerta", 1);
			result= false;
		}
			
		if(!correoValidator(correo)) {
			JOptionPane.showMessageDialog(null, "El campo correo no respeta el formato:[caracteres alfanuméricos y signos]@[caracteres alfanuméricos y puntos]",
					"Alerta", 1);
			result=false;
		}		

		return result;

	}
	
	private boolean correoValidator(String correo) {
		 String regex="^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
	        Pattern pattern= Pattern.compile(regex);
	        Matcher matcher= pattern.matcher(correo);

	        if(matcher.find()){
	            return true;
	        }
	        return false;
	}

	private  MouseAdapter limpiarCampoFechaEvnt () {
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				limpiarCampoFecha();
				
			}
		};
		
	}
	
	private void limpiarCampoFecha() {
		this.ventanaPersona.getDateChooser().setDate(null);
	}
	
	private KeyAdapter comprobarInput() {                                    	   
	    KeyAdapter keyAdapter=new KeyAdapter() {
	    	
				@Override
				public void keyTyped(KeyEvent e) {
					 int key = e.getKeyChar();

					    boolean numeros = key >= 48 && key <= 57;
					        
					    if (!numeros)
					    {
					        e.consume();
					    }
				}
			
	    };
	    
	    return keyAdapter;
	   
	} 
	
	private void cargarSignosZodiaco(){
		ArrayList<String> signos = new ArrayList<String>(Arrays.asList("Aries","Tauro","Geminis","Cancer","Leo","Virgo","Libra","Escorpio","Sagitario","Capricornio","Acuario","Piscis"));
				
		Collections.sort(signos);
		this.ventanaPersona.getComboBoxZodiacal().addItem("");
		for (String signo : signos) {
			this.ventanaPersona.getComboBoxZodiacal().addItem(signo);
		}
		
		
	}
	
	private void cargarPreferenciasMusicales() {
		ArrayList<String> preferencias = new ArrayList<String>(Arrays.asList("Rock","Electronica","Cumbia","Folclore","Otros"));
		
		this.ventanaPersona.getComboBoxMusica().addItem("");
		for (String preferencia : preferencias) {
			this.ventanaPersona.getComboBoxMusica().addItem(preferencia);
		}
		
	}
	private void cargarPaises() {
		JSONReader controladorJSON = new JSONReader();
		ArrayList<String> paises = controladorJSON.getPaises();
		Collections.sort(paises);

		this.ventanaPersona.getComboBoxPais().addItem("");
		for (String pais : paises) {
			this.ventanaPersona.getComboBoxPais().addItem(pais);
		}
	}

	private void cargarProvincias() {
		JSONReader controladorJSON = new JSONReader();
		ArrayList<String> provincias = controladorJSON.getProvincias();
		Collections.sort(provincias);

		this.ventanaPersona.getComboBoxProvincia().addItem("");
		for (String provincia : provincias) {
			this.ventanaPersona.getComboBoxProvincia().addItem(provincia);
		}

	}

	private void cargarLocalidades(ActionEvent e) {

		this.ventanaPersona.getComboBoxLocalidades().removeAllItems();
		JSONReader controladorJSON = new JSONReader();
		if (this.ventanaPersona.getComboBoxProvincia().getSelectedItem() != null) {
			ArrayList<String> localidades = controladorJSON
					.getLocalidades(this.ventanaPersona.getComboBoxProvincia().getSelectedItem().toString());
			Collections.sort(localidades);
			cargarComboboxLocalidades(localidades);
		}
	}

	public void cargarComboboxLocalidades(ArrayList<String> localidades) {
		for (String localidad : localidades) {
			this.ventanaPersona.getComboBoxLocalidades().addItem(localidad);
		}
	}

	public void cargarTipo() {
		ArrayList<String> tipos = new ArrayList<String>(Arrays.asList("Amigos", "Familia", "Trabajo", "Otros"));
		this.ventanaPersona.getComboBoxTipo().addItem("");
		for (String tipo : tipos) {
			this.ventanaPersona.getComboBoxTipo().addItem(tipo);
		}
	}
	
	private PropertyChangeListener comprobarSigno() {
		return new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				try {
					
					definirSignoZodiaco();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		};
		
	}
	
	private void definirSignoZodiaco() throws ParseException {
		JSONReader jsonReader=new JSONReader();
		if(this.ventanaPersona.getDateChooser().getDate()!=null) {	
			String signo=jsonReader.getSignoZodiaco(this.ventanaPersona.getDateChooser().getDate());
			this.ventanaPersona.getComboBoxZodiacal().setSelectedItem(signo);
			this.ventanaPersona.habilitarComboZodiaco(false);
		}else {
			this.ventanaPersona.habilitarComboZodiaco(true);
			this.ventanaPersona.getComboBoxZodiacal().setSelectedItem("");
		}
	}

	private void guardarEdicionPersona(ActionEvent e) {
		String nombre = this.ventanaPersona.getTxtNombre().getText();
		String tel = "";
		if (!this.ventanaPersona.getTxtTelefono().getText().isEmpty()) {
			tel = this.ventanaPersona.getTxtTelefono().getText();
		}

		String correo = this.ventanaPersona.getTextCorreo().getText();		
		
		Date cumpleanios =new Date(0000,00,00);
		if(this.ventanaPersona.getDateChooser().getDate()!=null) {
			cumpleanios=this.ventanaPersona.getDateChooser().getDate();	
		}
		
		String tipo = this.ventanaPersona.getComboBoxTipo().getSelectedItem().toString();
		String pais = "";
		String provincia = "";
		String localidad = "";

		if (this.ventanaPersona.getComboBoxPais().getSelectedItem() != null) {
			pais = this.ventanaPersona.getComboBoxPais().getSelectedItem().toString();

		}

		if (this.ventanaPersona.getComboBoxProvincia().getSelectedItem() != null
				&& this.ventanaPersona.getComboBoxLocalidades().getSelectedItem() != null) {
			provincia = this.ventanaPersona.getComboBoxProvincia().getSelectedItem().toString();
			localidad = this.ventanaPersona.getComboBoxLocalidades().getSelectedItem().toString();

		}

		String calle ="";		
		if(this.ventanaPersona.getTextCalle().getText() != null) {
			calle= this.ventanaPersona.getTextCalle().getText();
		}
		
		Integer altura =-1;
		if(!this.ventanaPersona.getTextAltura().getText().isEmpty()) {
			System.out.println("Altura edit "+this.ventanaPersona.getTextAltura().getText());
			altura=Integer.parseInt(this.ventanaPersona.getTextAltura().getText());
		}
		
		String depto ="";
		if(this.ventanaPersona.getTextCalle().getText() != null) {
			depto= this.ventanaPersona.getTextDepto().getText();
		}
		
		String signo="No especificado";
		if (this.ventanaPersona.getComboBoxZodiacal().getSelectedItem()!="") {
			signo = this.ventanaPersona.getComboBoxZodiacal().getSelectedItem().toString();

		}
			
		
		String musicaPreferida="Otros";
		if (this.ventanaPersona.getComboBoxMusica().getSelectedItem()!="") {
			musicaPreferida = this.ventanaPersona.getComboBoxMusica().getSelectedItem().toString();

		}
		

		if (puedeGuardar(nombre, tel + "",correo,altura)) {
			
			PersonaDTO nuevaPersona = new PersonaDTO(personaEditar.getIdPersona(), nombre, tel, correo, cumpleanios, tipo, pais, provincia,
					localidad, calle, altura, depto,signo,musicaPreferida);

			this.agenda.editarPersona(nuevaPersona);
			this.refrescarTabla();
			this.ventanaPersona.limpiarVentana();
		}

	}

	private void mostrarReporte(ActionEvent r) {
		String tipoReporte=this.ventanaGenerarReporte.getComboBoxTipoReporte().getSelectedItem().toString();
		String ordenamiento=this.ventanaGenerarReporte.getComboBoxOrdenamiento().getSelectedItem().toString();
		
		if(!tipoReporte.equals("")) {			
			ReporteAgenda reporte = new ReporteAgenda(tipoReporte,ordenamiento);
			reporte.mostrar();
		}else {
			JOptionPane.showMessageDialog(null, "Debe elegir un tipo de reporte para continuar.",
					"Alerta Reporte", 1);
			
		}
	}
	
	private void mostrarVentanaReporte(ActionEvent r) {
		this.ventanaGenerarReporte.mostrarVentana();
		
	}
	
	private void comprobarTipoReporte(ActionEvent r) {
		if(this.ventanaGenerarReporte.getComboBoxTipoReporte().getSelectedItem().equals("Preferencias musicales")) {
			this.ventanaGenerarReporte.habilitarOrdenamiento(true);
		}else {
			this.ventanaGenerarReporte.habilitarOrdenamiento(false);
		}
	}

	private void habilitarProvincias(ActionEvent e) {
		if (this.ventanaPersona.getComboBoxPais().getSelectedItem().toString().equals("Argentina")) {
			this.ventanaPersona.habilitarProvinciasLocalidades(true);
			cargarProvincias();
		} else {
			this.ventanaPersona.habilitarProvinciasLocalidades(false);
		}
	}

	public void borrarPersona(ActionEvent s) {
		int filasSeleccionada = this.vista.getTablaPersonas().getSelectedRow();
				
		if(filasSeleccionada==-1) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar algun contacto de la lista.",
					"Alerta", 1);
		}else {
						
			this.agenda.borrarPersona(this.personasEnTabla.get(filasSeleccionada));
			
			this.refrescarTabla();
		}
	}

	private Object editarPersona(ActionEvent m) {
		this.ventanaPersona.limpiarVentana();
		this.ventanaPersona.mostrarBotonGuardar();
		cargarSignosZodiaco();
		cargarPreferenciasMusicales();
		cargarTipo();

		int fila =-1;
		if(this.vista.getTablaPersonas().getSelectedRow()==-1) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar algun contacto de la lista.",
					"Alerta", 1);
		}else {
			fila = this.vista.getTablaPersonas().getSelectedRow();
				
			personaEditar = this.personasEnTabla.get(fila);
			JSONReader controladorJSON = new JSONReader();
	
			if (personaEditar.getPais().trim().equals("Argentina")) {
				cargarProvincias();
			}
	
			controladorJSON = new JSONReader();
			ArrayList<String> localidades = controladorJSON.getLocalidades(personaEditar.getProvincia().trim());
			Collections.sort(localidades);
	
			cargarComboboxLocalidades(localidades);
			this.ventanaPersona.mostrarVentana();
			this.ventanaPersona.llenarCamposParaEditar(personaEditar);
		}
		return null;
	}

	public void inicializar() throws IOException {
		this.vista.show();		
		
		if(Encriptacion.leerContrasenia().isEmpty()) {
			ControladorRegistro contrRegistro=new ControladorRegistro(this);	
			contrRegistro.mostrarVentana(true);
		}else {		
			this.refrescarTabla();
		}
	}

	public void refrescarTabla() {
		this.personasEnTabla = agenda.obtenerPersonas();
		this.vista.llenarTabla(this.personasEnTabla);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
