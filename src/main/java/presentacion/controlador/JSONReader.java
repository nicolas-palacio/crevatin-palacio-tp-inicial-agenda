package presentacion.controlador;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JSONReader {
	
	public ArrayList<String> getPaises(){
		ArrayList<String> listaPaises = new ArrayList<>();
		Path pathPaises = Paths.get("json/paises.json");

		try (Reader reader = Files.newBufferedReader(pathPaises, StandardCharsets.UTF_8)) {
			final JsonParser parser = new JsonParser();
			final JsonElement tree = parser.parse(reader);
			final JsonObject fileObj = tree.getAsJsonObject();

			final JsonArray array = fileObj.get("countries").getAsJsonArray();

			for (final JsonElement element : array) {
				if (element.isJsonObject()) {
					final JsonObject car = element.getAsJsonObject();
					final String provincia = car.get("name").toString();
					StringBuilder nuevaString = new StringBuilder(provincia);
					nuevaString.deleteCharAt(0);
					nuevaString.deleteCharAt(nuevaString.length() - 1);
					
					listaPaises.add(nuevaString.toString());
				}
			}
		} catch (final IOException e) {
			
			e.printStackTrace();
		}

		return listaPaises;
		
	}

	public ArrayList<String> getProvincias() { 

		ArrayList<String> listaProvincias = new ArrayList<>();
		Path path = Paths.get("json/provincias.json");

		try (Reader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
			final JsonParser parser = new JsonParser();
			final JsonElement tree = parser.parse(reader);
			final JsonObject fileObj = tree.getAsJsonObject();

			final JsonArray array = fileObj.get("provincias").getAsJsonArray();

			for (final JsonElement element : array) {
				if (element.isJsonObject()) {
					final JsonObject car = element.getAsJsonObject();
					final String provincia = car.get("nombre").toString();
					StringBuilder nuevaString = new StringBuilder(provincia);
					nuevaString.deleteCharAt(0);
					nuevaString.deleteCharAt(nuevaString.length() - 1);


					listaProvincias.add(nuevaString.toString());
				}
			}
		} catch (final IOException e) {
			
			e.printStackTrace();
		}

		return listaProvincias;
	}

	public ArrayList<String> getLocalidades(String provincia) {
		ArrayList<String> listaLocalidades = new ArrayList();
		Path pathLocalidades = Paths.get("json/localidades.json");

		try (Reader reader = Files.newBufferedReader(pathLocalidades, StandardCharsets.UTF_8)) {
			final JsonParser parser = new JsonParser();
			final JsonElement tree = parser.parse(reader);
			final JsonObject fileObj = tree.getAsJsonObject();

			final JsonArray array = fileObj.get("localidades-censales").getAsJsonArray();

			for (final JsonElement element : array) {
				StringBuilder nuevaString = new StringBuilder(
						element.getAsJsonObject().get("provincia").getAsJsonObject().get("nombre").toString());
				nuevaString.deleteCharAt(0);
				nuevaString.deleteCharAt(nuevaString.length() - 1);

				if (element.isJsonObject() && nuevaString.toString().equals(provincia)) {
					final JsonElement localidad = element.getAsJsonObject().get("nombre");
					StringBuilder localidadSB = new StringBuilder(localidad.toString());
					localidadSB.deleteCharAt(0);
					localidadSB.deleteCharAt(localidadSB.length() - 1);
					
					
					listaLocalidades.add(localidadSB.toString());
				}

			}
		} catch (final IOException e) {

			e.printStackTrace();
		}

		return listaLocalidades;

	}

	public String getSignoZodiaco(Date date) throws ParseException {
		ArrayList<String> signos = new ArrayList();
		Path pathSignos = Paths.get("json/signosZodiaco.json");
		
		
		try (Reader reader = Files.newBufferedReader(pathSignos, StandardCharsets.UTF_8)) {
			final JsonParser parser = new JsonParser();
			final JsonElement tree = parser.parse(reader);
			
			final JsonArray array = tree.getAsJsonArray();
						
			for (final JsonElement element : array) {
				String periodoInicioStr= element.getAsJsonObject().get("periodo").getAsJsonArray().get(0).toString();
				String periodoFinStr= element.getAsJsonObject().get("periodo").getAsJsonArray().get(1).toString();
				
				StringBuilder periodoInicioSB = new StringBuilder(periodoInicioStr);
				periodoInicioSB.deleteCharAt(0);
				periodoInicioSB.deleteCharAt(periodoInicioSB.length() - 1);
				
				StringBuilder periodoFinSB = new StringBuilder(periodoFinStr);
				periodoFinSB.deleteCharAt(0);
				periodoFinSB.deleteCharAt(periodoFinSB.length() - 1);
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");  
				Date dateSeleccionada=sdf.parse(sdf.format(date));
				Date periodoInicio=sdf.parse(periodoInicioSB.toString());
				Date periodoFin=sdf.parse(periodoFinSB.toString());
				boolean bool=dateSeleccionada.after(periodoInicio) && dateSeleccionada.before(periodoFin);
				boolean boolFechIguales=dateSeleccionada.equals(periodoInicio) || dateSeleccionada.equals(periodoFin) ;
				
						
				if (bool || boolFechIguales) {
					final JsonElement signo = element.getAsJsonObject().get("nombre");
					StringBuilder signoSB = new StringBuilder(signo.toString());
					signoSB.deleteCharAt(0);
					signoSB.deleteCharAt(signoSB.length() - 1);
										
					return signoSB.toString();
				}

			}
		} catch (final IOException e) {

			e.printStackTrace();
		}
		
		
		return "";
	}
}
