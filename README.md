**Trabajo Práctico Agenda**

Integrantes:
- Alan Xavier Crevatin
- Nicolas Fabian Ezequiel Palacio

El objetivo es desarrollar un software que permita el almacenamiento de contactos a modo de “agenda”.

Se  requiere poder agregar los datos de un contacto con su domicilio (calle, altura, piso, depto. y localidad), su dirección de email, su fecha de cumpleaños, y una etiqueta que represente el tipo de contacto (trabajo, familia, amigos, etc.). El tipo de contacto y la localidad deben poder elegirse de una lista desplegable y debe realizarse el correspondiente ABM. La localidad corresponde a una provincia y ésta a un país.

Finalmente, se entregará un instalador que permitirá instalar el software por un usuario final en un sistema con Windows 7. Dicho instalador, debe instalar también el o los software necesario/s para la ejecución del sistema (JRE y el motor MySQL). El instalador debe mostrarse sobre una máquina virtual con sólo el SO instalado. El instalador debe permitir instalar el sistema en cualquier destino de una PC y el sistema debe permitir configurar el servidor, el usuario y la contraseña para conectarse al motor de base de datos según lo desee el usuario final.
