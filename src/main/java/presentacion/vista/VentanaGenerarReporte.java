package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.border.EmptyBorder;

import javax.swing.JComboBox;


public class VentanaGenerarReporte extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static VentanaGenerarReporte INSTANCE;
	private JLabel lblTipoReporte;
	private JLabel lblOrdenamiento;
	private JComboBox comboBoxTipoReporte;
	private JComboBox comboBoxOrdenamiento;
	private JButton btnGenerar;


	public static VentanaGenerarReporte getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new VentanaGenerarReporte();
			return new VentanaGenerarReporte();
		} else
			return INSTANCE;
	}

	private VentanaGenerarReporte() {
		super();
		setResizable(false);
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 377, 270);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 345, 213);
		contentPane.add(panel);
		panel.setLayout(null);

		lblTipoReporte = new JLabel("Tipo de Reporte");
		lblTipoReporte.setBounds(27, 75, 113, 14);
		panel.add(lblTipoReporte);

		lblOrdenamiento = new JLabel("Ordenamiento");
		lblOrdenamiento.setBounds(27, 127, 113, 14);
		panel.add(lblOrdenamiento);

		comboBoxTipoReporte = new JComboBox();
		comboBoxTipoReporte.setBounds(133, 72, 164, 20);
		panel.add(comboBoxTipoReporte);

		comboBoxOrdenamiento = new JComboBox();
		comboBoxOrdenamiento.setBounds(133, 124, 164, 20);
		panel.add(comboBoxOrdenamiento);
		comboBoxOrdenamiento.setEnabled(false);
		
		btnGenerar = new JButton("Generar");
		btnGenerar.setBounds(234, 179, 89, 23);
		panel.add(btnGenerar);
		
		cargarDatos();
		
		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	private void cargarDatos() {
		this.comboBoxOrdenamiento.addItem("");
		this.comboBoxOrdenamiento.addItem("Ascendente");
		this.comboBoxOrdenamiento.addItem("Descendente");
		
		this.comboBoxTipoReporte.addItem("");
		this.comboBoxTipoReporte.addItem("Signos del Zodiaco");
		this.comboBoxTipoReporte.addItem("Preferencias musicales");
		
	}
	
	public void habilitarOrdenamiento(boolean habilitacion) {
		this.comboBoxOrdenamiento.setEnabled(habilitacion);
		if(!habilitacion) {
			this.comboBoxOrdenamiento.setSelectedItem("");
		}
	}

	public JButton getBtnGenerar() {
		return btnGenerar;
	}

	public JComboBox getComboBoxTipoReporte() {
		return comboBoxTipoReporte;
	}

	public JComboBox getComboBoxOrdenamiento() {
		return comboBoxOrdenamiento;
	}
		
	
	
}
