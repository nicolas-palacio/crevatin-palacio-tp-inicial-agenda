package persistencia.conexion;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import presentacion.controlador.seguridad.Encriptacion;

import org.apache.ibatis.jdbc.ScriptRunner;




public class Conexion 
{
	public static Conexion instancia;
	private Connection connection;
	private Logger log = LogManager.getLogger(Conexion.class);
	private String password;
	
	private Conexion(){
		try {
			this.password=Encriptacion.leerContrasenia();
		} catch (IOException e1) {
			
			e1.printStackTrace();
		}
		if(!this.password.equals("")) {
			crearBDAgenda();
			try{
				
				Class.forName("org.postgresql.Driver");
				this.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/agenda","postgres",this.password);
				
				if(connection!=null) {			
					ScriptRunner sr= new ScriptRunner(this.connection);
					
					Reader reader=new BufferedReader(new FileReader("sql/scriptAgenda.sql"));
					sr.runScript(reader);
					System.out.println("Conexion exitosa");
					log.info("Conexion exitosa");
				}else {
					System.out.println("Conexion fallida");
				}	
				
			}
			catch(Exception e)
			{
				log.error("Conexion fallida", e);
			}
		}
		
	}
	
	private void crearBDAgenda() {
			try{
			
			Class.forName("org.postgresql.Driver");
			this.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres",this.password);
			
			if(connection!=null) {			
				String query= "create database agenda";
				Statement stmt= this.connection.createStatement();
				stmt.execute(query);
				
				System.out.println("Exito al crear la base de datos Agenda");
				this.cerrarConexion();
			}else {
				System.out.println("Fallo al crear la base de datos Agenda");
			}	
			
		}catch(Exception e){
			System.out.println("Error");
				
			}
	}
	
	
	public static Conexion getConexion()   
	{								
		if(instancia == null)
		{
			instancia = new Conexion();
		}
		return instancia;
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public void setPassword(String password) {
		this.password=password;
		instancia = new Conexion();
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			log.info("Conexion cerrada");
		}
		catch (SQLException e) 
		{
			log.error("Error al cerrar la conexión!", e);
		}
		instancia = null;
	}
	
}
