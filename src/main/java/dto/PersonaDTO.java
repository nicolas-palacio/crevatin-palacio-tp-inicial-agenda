package dto;

import java.util.Date;

public class PersonaDTO 
{
	private int idPersona;
	private String nombre;
	private String telefono;
	private String correo;
	private Date cumpleanios;
	private String tipo;
	private String pais;
	private String provincia;
	private String localidad;
	private String calle;
	private int altura;
	private String depto;
	private String signoZodiacal;
	private String musicaPreferida;
	
	
	public PersonaDTO(int idPersona,String nombre, String telefono, String correo, Date cumpleanios, String tipo, String pais,
			String provincia, String localidad, String calle, int altura, String depto, String signoZodiacal,
			String musicaPreferida) {
		super();
		
		this.idPersona=idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.correo = correo;
		this.cumpleanios = cumpleanios;
		this.tipo = tipo;
		this.pais = pais;
		this.provincia = provincia;
		this.localidad = localidad;
		this.calle = calle;
		this.altura = altura;
		this.depto = depto;
		this.signoZodiacal = signoZodiacal;
		this.musicaPreferida = musicaPreferida;
	}


	public int getIdPersona() {
		return idPersona;
	}



	public String getNombre() {
		return nombre;
	}



	public String getTelefono() {
		return telefono;
	}



	public String getCorreo() {
		return correo;
	}



	public Date getCumpleanios() {
		return cumpleanios;
	}



	public String getTipo() {
		return tipo;
	}



	public String getPais() {
		return pais;
	}


	public String getProvincia() {
		return provincia;
	}

	public String getLocalidad() {
		return localidad;
	}


	public String getSignoZodiacal() {
		return signoZodiacal;
	}


	public String getMusicaPreferida() {
		return musicaPreferida;
	}


	public String getCalle() {
		return calle;
	}


	public int getAltura() {
		return altura;
	}



	public String getDepto() {
		return depto;
	}
	
	
}
