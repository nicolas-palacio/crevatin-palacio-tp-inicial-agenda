package presentacion.controlador.seguridad;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;



public class Encriptacion {
	private static final int key=128;
	
	
	public static void escribirContrasenia(String pass) throws IOException {
		Path path = Paths.get(System.getProperty("user.home").toString()+"\\passwordSQL.txt");
		FileWriter fw=new FileWriter(path.toString());
		
		ArrayList<Integer>arr=encriptarString(pass);
		
		for(int i=0;i<arr.size();i++) {
			if(i==arr.size()-1) {
				fw.write(arr.get(i).toString());
			}else {				
				fw.write(arr.get(i).toString()+",");
			}
		}
		
		fw.close();		
		
	}
	
	public static String leerContrasenia() throws IOException {
		
		String sb="";
		
		int i;
		
		FileReader fr=null;
		
		try {
			Path path = Paths.get(System.getProperty("user.home").toString()+"\\passwordSQL.txt");
			File file=new File(path.toString());
			if(!file.exists()) {
				file.createNewFile();
			}
			
			fr=new FileReader(path.toString());
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while((i=fr.read())!=-1) {
			char c=(char)i;
			sb+=c;			
		}
	
		String[] caracteresAscii=sb.toString().split(",");
		
		if(sb.isEmpty()) {
			return "";
		}
		return decryptString(caracteresAscii);
	}
	
	
	public static ArrayList<Integer>  encriptarString(String password) {
		ArrayList<Integer> arr=new ArrayList<>();
		
		
		for(int c=0;c<password.length();c++) {			
			int asciiChar=(int)password.charAt(c)+key;
			arr.add(asciiChar);
		}
				
		
		return arr;
		
	}
	
	public static String decryptString(String[] array) {
		String contrasenia="";
		
		for(int i=0;i<array.length;i++) {
				int asciiChar=Integer.parseInt(array[i])-key;
				char caracter=(char)asciiChar;
				contrasenia+=caracter;				
			}				
		
		System.out.println("Contrasenia "+contrasenia);
		return contrasenia;
	}
	
	public static void limpiarContrasenia() throws IOException {
		Path path = Paths.get(System.getProperty("user.home").toString()+"\\passwordSQL.txt");
		File file=new File(path.toString());
		
		file.delete();
		
		file.createNewFile();
		
	}
	


}
