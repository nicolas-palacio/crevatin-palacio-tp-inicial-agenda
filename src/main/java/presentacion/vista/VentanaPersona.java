package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import javax.swing.JComboBox;

import com.toedter.calendar.JDateChooser;

import dto.PersonaDTO;
import java.util.Date;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.ImageIcon;

public class VentanaPersona extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JButton btnAgregarPersona;
	private JButton btnGuardarCambios;
	private JButton btnLimpiarFecha;
	private static VentanaPersona INSTANCE;
	private JLabel lblProvincia;
	private JLabel lblLocalidad;
	private JComboBox comboBoxPais;
	private JComboBox comboBoxProvincia;
	private JComboBox comboBoxLocalidades;
	private JComboBox comboBoxTipo;
	private JTextField textCorreo;
	private JTextField textCalle;
	private JTextField textAltura;
	private JTextField textDepto;
	private JDateChooser dateChooser;
	private JComboBox comboBoxZodiacal;
	private JComboBox comboBoxMusica;


	public static VentanaPersona getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new VentanaPersona();
			return new VentanaPersona();
		} else
			return INSTANCE;
	}

	private VentanaPersona() {
		super();
		
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 377, 596);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 345, 532);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 11, 113, 14);
		panel.add(lblNombreYApellido);

		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 48, 113, 14);
		panel.add(lblTelfono);

		txtNombre = new JTextField();
		txtNombre.setBounds(133, 8, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		txtTelefono = new JTextField();		
		txtTelefono.setBounds(133, 45, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);

		btnAgregarPersona = new JButton("Agregar");
		btnAgregarPersona.setBounds(208, 496, 89, 23);
		panel.add(btnAgregarPersona);

		JLabel lblPais = new JLabel("Pais");
		lblPais.setBounds(10, 202, 113, 14);
		panel.add(lblPais);

		lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(10, 242, 113, 14);
		panel.add(lblProvincia);

		lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 284, 113, 14);
		panel.add(lblLocalidad);

		comboBoxPais = new JComboBox();
		comboBoxPais.setBounds(133, 199, 164, 20);
		panel.add(comboBoxPais);

		comboBoxProvincia = new JComboBox();
		comboBoxProvincia.setBounds(133, 239, 164, 20);
		panel.add(comboBoxProvincia);

		comboBoxLocalidades = new JComboBox();
		comboBoxLocalidades.setBounds(133, 281, 164, 20);
		panel.add(comboBoxLocalidades);
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setBounds(10, 86, 113, 14);
		panel.add(lblCorreo);
		
		textCorreo = new JTextField();
		textCorreo.setColumns(10);
		textCorreo.setBounds(133, 83, 164, 20);
		panel.add(textCorreo);
		
		JLabel lblNacimiento = new JLabel("Nacimiento");
		lblNacimiento.setBounds(10, 122, 113, 14);
		panel.add(lblNacimiento);
		
		JLabel lblTipoDeContacto = new JLabel("Tipo de contacto");
		lblTipoDeContacto.setBounds(10, 161, 113, 14);
		panel.add(lblTipoDeContacto);
		
		comboBoxTipo = new JComboBox();
		comboBoxTipo.setBounds(133, 157, 164, 20);
		panel.add(comboBoxTipo);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 323, 113, 14);
		panel.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 354, 113, 14);
		panel.add(lblAltura);
		
		JLabel lblPisoYDepto = new JLabel("Piso y Depto.");
		lblPisoYDepto.setBounds(10, 390, 113, 14);
		panel.add(lblPisoYDepto);
		
		textCalle = new JTextField();
		textCalle.setColumns(10);
		textCalle.setBounds(133, 320, 164, 20);
		panel.add(textCalle);
		
		textAltura = new JTextField();
		textAltura.setColumns(10);
		textAltura.setBounds(133, 351, 164, 20);
		panel.add(textAltura);		
		
		textDepto = new JTextField();
		textDepto.setColumns(10);
		textDepto.setBounds(133, 387, 164, 20);
		panel.add(textDepto);
		
		dateChooser = new JDateChooser();		
			
		dateChooser.setBounds(133, 122, 164, 20);
		panel.add(dateChooser);
		
		btnGuardarCambios = new JButton("Guardar");
		btnGuardarCambios.setBounds(208, 496, 89, 23);
		panel.add(btnGuardarCambios);
		
		JLabel lblSignoZodiaco = new JLabel("Signo Zodiaco");
		lblSignoZodiaco.setBounds(10, 430, 113, 14);
		panel.add(lblSignoZodiaco);
		
		comboBoxZodiacal = new JComboBox();
		comboBoxZodiacal.setBounds(133, 426, 164, 22);
		panel.add(comboBoxZodiacal);
		
		JLabel lblPreferenciaMusical = new JLabel("Preferencia Musical");
		lblPreferenciaMusical.setBounds(10, 467, 113, 14);
		panel.add(lblPreferenciaMusical);
		
		comboBoxMusica = new JComboBox();
		comboBoxMusica.setBounds(133, 463, 164, 22);
		panel.add(comboBoxMusica);
		
		btnLimpiarFecha = new JButton("");
		Path pathIcon = Paths.get("icon/deleteIcon.png");
		btnLimpiarFecha.setIcon(new ImageIcon(pathIcon.toString()));
		btnLimpiarFecha.setBounds(307, 119, 28, 23);
		panel.add(btnLimpiarFecha);
		
		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}
	
	public void llenarCamposParaEditar(PersonaDTO persona) {
		txtNombre.setText(persona.getNombre().trim());
		txtTelefono.setText(persona.getTelefono().trim());
		textCorreo.setText(persona.getCorreo().trim());
		dateChooser.setDate(persona.getCumpleanios());
		comboBoxTipo.setSelectedItem(persona.getTipo().trim());
		comboBoxPais.setSelectedItem(persona.getPais().trim());
		comboBoxProvincia.setSelectedItem(persona.getProvincia().trim());
		comboBoxLocalidades.setSelectedItem(persona.getLocalidad().trim());
		comboBoxZodiacal.setSelectedItem(persona.getSignoZodiacal().trim());
		comboBoxMusica.setSelectedItem(persona.getMusicaPreferida().trim());
		
		textCalle.setText(persona.getCalle().trim());
		
		
		if(fechaVacia(persona.getCumpleanios())) {
			dateChooser.setDate(null);
		}
		
		if(persona.getAltura()==-1) {
			textAltura.setText("");
		}else {
			
			textAltura.setText(Integer.toString(persona.getAltura()));
		}
		textDepto.setText(persona.getDepto().trim());
			
	}
	
	private boolean fechaVacia(Date fecha) {
		Date fechaAux=new Date(0000,00,0);
		
		return fecha.equals(fechaAux);
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public JButton getBtnAgregarPersona() {
		return btnAgregarPersona;
	}
		
	
	public JButton getBtnLimpiarFecha() {
		return btnLimpiarFecha;
	}

	public JTextField getTextCorreo() {
		return textCorreo;
	}

	public JTextField getTextCalle() {
		return textCalle;
	}

	public JTextField getTextAltura() {
		return textAltura;
	}

	public JTextField getTextDepto() {
		return textDepto;
	}

	public JDateChooser getDateChooser() {
		return dateChooser;
	}
	
	public JComboBox getComboBoxTipo() {
		return comboBoxTipo;
	}

	public JComboBox getComboBoxPais() {
		return comboBoxPais;
	}

	public JComboBox getComboBoxProvincia() {
		return comboBoxProvincia;
	}

	public JComboBox getComboBoxLocalidades() {
		return comboBoxLocalidades;
	}
		
	public JButton getBtnGuardarCambios() {
		return btnGuardarCambios;
	}
	
	public JComboBox getComboBoxZodiacal() {
		return comboBoxZodiacal;
	}

	public JComboBox getComboBoxMusica() {
		return comboBoxMusica;
	}

	public void habilitarProvinciasLocalidades(boolean habilitacion) {
		if(!habilitacion) {
			this.comboBoxProvincia.removeAllItems();
			this.comboBoxLocalidades.removeAllItems();
		}		
		this.comboBoxProvincia.setEnabled(habilitacion);
		this.comboBoxLocalidades.setEnabled(habilitacion);
		
	}
	
	public void habilitarComboZodiaco(boolean habilitacion) {
		this.comboBoxZodiacal.setEnabled(habilitacion);
		
	}

	public void mostrarBotonAgregar() {
		this.btnGuardarCambios.setVisible(false);
		this.btnAgregarPersona.setVisible(true);
	}
	
	public void mostrarBotonGuardar() {
		this.btnGuardarCambios.setVisible(true);
		this.btnAgregarPersona.setVisible(false);
	}

	public void limpiarVentana() {
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.textCorreo.setText(null);
		this.dateChooser.setDate(null);
		this.comboBoxTipo.removeAllItems();		
		this.comboBoxPais.setSelectedItem("");
		this.comboBoxProvincia.removeAllItems();
		this.comboBoxLocalidades.removeAllItems();
		this.comboBoxZodiacal.removeAllItems();
		this.comboBoxMusica.removeAllItems();
		this.textCalle.setText(null);
		this.textAltura.setText(null);
		this.textDepto.setText(null);
		this.dispose();
	}
}
