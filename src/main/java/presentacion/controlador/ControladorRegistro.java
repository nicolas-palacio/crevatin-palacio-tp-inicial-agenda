package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;

import javax.swing.JOptionPane;

import persistencia.conexion.Conexion;
import presentacion.controlador.seguridad.Encriptacion;
import presentacion.vista.VentanaLogin;


public class ControladorRegistro {
	private VentanaLogin ventanaLogin;
	private Controlador contr;
	private Connection conexion;
	
	public ControladorRegistro(Controlador contr) throws IOException {
		this.contr=contr;
		this.conexion = Conexion.getConexion().getSQLConexion();		
		this.ventanaLogin= new VentanaLogin();
		
		this.ventanaLogin.getBtnLogin().addActionListener(a->{
			try {
				guardarContrasenia(a);
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		});						
		
	}
	
	public void mostrarVentana(boolean mostrar) {
		this.ventanaLogin.mostrarVentana(mostrar);
	}

	
	public boolean digitoLaContrasenia() throws IOException {
		return Encriptacion.leerContrasenia().isEmpty();
	}
	
	private void guardarContrasenia(ActionEvent a) throws IOException {
		if(this.ventanaLogin.getTextFieldPassword().getText().toString().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Debe ingresar la contraseņa.",
					"Alerta Contraseņa", 1);
		}else {
			Encriptacion.escribirContrasenia(this.ventanaLogin.getTextFieldPassword().getText().toString());		
			
			Conexion.instancia.setPassword(this.ventanaLogin.getTextFieldPassword().getText().toString());
			this.conexion = Conexion.getConexion().getSQLConexion();
			
		
			
			this.ventanaLogin.mostrarVentana(false);		
			if(conexion==null) {
				JOptionPane.showMessageDialog(null, "Contraseņa invalida.Por favor, ingresela nuevamente.",
						"Alerta Contraseņa", 1);
				this.ventanaLogin.getTextFieldPassword().setText("");		
				reintetarContrasenia();
				
			}else {
				this.contr.refrescarTabla();
			}
		}
								
	}

	private void reintetarContrasenia() throws IOException {		
		Encriptacion.limpiarContrasenia();		
		this.ventanaLogin.mostrarVentana(true);
		
	}

}
