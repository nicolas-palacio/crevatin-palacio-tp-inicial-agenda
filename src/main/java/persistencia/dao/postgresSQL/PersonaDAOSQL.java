package persistencia.dao.postgresSQL;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas VALUES(default,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String edit = "UPDATE personas SET nombre=?,telefono=?,correo=?,fech_cumpleanios=?,tipo=?,pais=?,provincia=?,localidad=?,calle=?,altura=?,depto=?,signoZodiaco=?,musicaPreferida=?  WHERE idPersona=?";
	private static final String readall = "SELECT * FROM personas";
		
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
						
			Date sqlDate = new Date(persona.getCumpleanios().getTime());										
			
			statement = conexion.prepareStatement(insert);
			statement.setString(1, persona.getNombre());
			statement.setString(2, persona.getTelefono());
			statement.setString(3, persona.getCorreo());
			statement.setDate(4,sqlDate);
			statement.setString(5, persona.getTipo());
			statement.setString(6, persona.getPais());
			statement.setString(7, persona.getProvincia());
			statement.setString(8, persona.getLocalidad());
			statement.setString(9, persona.getCalle());
			statement.setInt(10, persona.getAltura());
			statement.setString(11, persona.getDepto());
			statement.setString(12, persona.getSignoZodiacal());
			statement.setString(13, persona.getMusicaPreferida());
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar){
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			String deleteQuery=String.format("DELETE FROM personas WHERE idPersona=%s",Integer.toString(persona_a_eliminar.getIdPersona()));
			statement = conexion.prepareStatement(deleteQuery);			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public boolean edit(PersonaDTO persona) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditExitoso = false;
		try 
		{
			Date sqlDate = new Date(persona.getCumpleanios().getTime());												
			statement = conexion.prepareStatement(edit);
			statement.setString(1, persona.getNombre());
			statement.setString(2, persona.getTelefono());
			statement.setString(3, persona.getCorreo());
			statement.setDate(4,sqlDate);
			statement.setString(5, persona.getTipo());
			statement.setString(6, persona.getPais());
			statement.setString(7, persona.getProvincia());
			statement.setString(8, persona.getLocalidad());
			statement.setString(9, persona.getCalle());
			statement.setInt(10, persona.getAltura());
			statement.setString(11, persona.getDepto());
			statement.setString(12, persona.getSignoZodiacal());
			statement.setString(13, persona.getMusicaPreferida());
			statement.setInt(14, persona.getIdPersona());
			
					
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isEditExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isEditExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("nombre");
		String tel = resultSet.getString("telefono");
		String correo= resultSet.getString("correo");
		Date cumpleanios=resultSet.getDate("fech_cumpleanios");
		String tipo=resultSet.getString("tipo");		
		String pais=resultSet.getString("pais");
		String provincia=resultSet.getString("provincia");
		String localidad=resultSet.getString("localidad");
		String calle=resultSet.getString("calle");
		int altura = resultSet.getInt("altura");
		String depto=resultSet.getString("depto");	
		String signo=resultSet.getString("signoZodiaco");	
		String musicaPreferida=resultSet.getString("musicaPreferida");	
			
		return new PersonaDTO(id,nombre,tel,correo,cumpleanios,tipo,pais,provincia,localidad,calle,altura,depto,signo,musicaPreferida);
	}
}
